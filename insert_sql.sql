use sdm;

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Rizki Saputra', 'L', 'Menikah', '10/11/1980', '1/1/2011', 1);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Farhan Reza', 'L', 'Belum', '11/1/1989', '1/1/2011', 1);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Riyando Adi', 'L', 'Menikah', '1/25/1977', '1/1/2011', 1);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Diego Manuel', 'L', 'Menikah', '2/22/1983', '9/4/2012', 2);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Satya Laksana', 'L', 'Menikah', '1/12/1981', '3/19/2011', 2);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Miguel Hernandez', 'L', 'Menikah', '10/16/1994', '6/15/2014', 2);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Putri Persada', 'P', 'Menikah', '1/30/1988', '4/14/2013', 2);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Alma Safira', 'P', 'Menikah', '5/18/1991', '9/28/2013', 3);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Haqi Hafiz', 'L', 'Belum', '9/19/1995', '3/9/2015', 3);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Abi Isyawara', 'L', 'Belum', '6/3/1991', '1/22/2012', 3);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Maman Kresna', 'L', 'Belum', '8/21/1993', '9/15/2012', 3);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Nadia Aulia', 'P', 'Belum', '10/7/1989', '5/7/2012', 4);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Mutiara Rezki', 'P', 'Menikah', '3/23/1988', '5/21/2013', 4);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Dani Setiawan', 'L', 'Belum', '2/11/1986', '11/30/2014', 4);

insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, Departemen)
VALUES('Budi Putra', 'L', 'Belum', '10/23/1995', '12/3/2015', 4);


insert into departemen (nama) value('Manajemen');
insert into departemen (nama) value('Pengembangan Bisnis');
insert into departemen (nama) value('Teknisi');
insert into departemen (nama) value('Analis');
