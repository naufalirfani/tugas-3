use sdm;

# insert into employee(nama, atasan_id, company_id) VALUES
#                                                          ('Pak Budi', null, 1),
#                                                          ('Pak Tono', 1, 1),
#                                                          ('Pak Totok', 1, 1),
#                                                          ('Bu Sinta', 2, 1),
#                                                          ('Bu Novi', 3, 1),
#                                                          ('Andre', 4, 1),
#                                                          ('Dono', 4, 1),
#                                                          ('Ismir', 5, 1),
#                                                          ('Anto', 5, 1);
#
# insert into company(nama, alamat) VALUES ('PT JAVAN', 'Sleman'), ('PT Dicoding', 'Bandung');

# CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO
select * from employee where atasan_id IS NULL;

# Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa
select * from employee where id NOT IN ( select atasan_id from employee where atasan_id is not null );

# Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur
select * from employee where atasan_id = (select id from employee where atasan_id IS NULL);

# Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager
select * from employee where atasan_id IN (select id from employee where atasan_id = (select id from employee where atasan_id IS NULL));

#Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama = Pak Budi
select count(*) from employee where atasan_id >= (select id from employee where nama = 'Pak Budi');

#Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama = Bu Sinta
select count(*) from employee where atasan_id = (select id from employee where nama = 'Bu Sinta');
